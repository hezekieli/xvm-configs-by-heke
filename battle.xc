﻿/* Heke's XVM Config - 11 June 2015
 * General parameters for the battle interface.
 */
{
  "battle": {
    // false - Disable tank icon mirroring (good for alternative icons).
    "mirroredVehicleIcons": true,
    // false - Disable pop-up panel at the bottom after death.
    "showPostmortemTips": true,
    // false - disable highlighting of own vehicle icon and squad.
    "highlightVehicleIcon": true,
    // true - enable {{spotted}} macro in players panels and minimap. WARNING: performance expensive
    "allowSpottedStatus": true,
    // true - enable {{hp*}} macros ability in players panels and minimap. WARNING: performance expensive
    "allowHpInPanelsAndMinimap": true,
    // true - enable {{marksOnGun}} macro in players panels and minimap. WARNING: performance expensive
    "allowMarksOnGunInPanelsAndMinimap": false,
    // Format of clock on the Debug Panel (near FPS).
    "clockFormat": "H:N", // TODO: "H:i"
    // Path to clan icons folder relative to res_mods/xvm/res.
    "clanIconsFolder": "clanicons/",
    // Path to sixth sense icon ("" for original icon).
    "sixthSenseIcon": "xvm://res/SixthSense.png",
    // GUI elements settings (experts only)
    "elements": ${"elements.xc":"elements"}
  },
  // Frag counter panel at top side of battle windows interface.
  // ?????? ????? ? ???.
  "fragCorrelation": {
    // true - show quantity of alive instead of dead
    // true - ?????????? ?????????? ????? ?????? ?????? ??????
    "showAliveNotFrags": false
  },
  // Ingame crits panel by "expert" skill.
  // ????????????? ?????? ?????? ?? ?????? "???????".
  "expertPanel": {
    // Delay for panel disappear. Original value was 5.
    // ???????? ???????????? ??????. ???????????? ???????? ???? 5.
    "delay": 15,
    // Panel scaling. Original value was 100.
    // ?????????? ??????. 100 ? ?????????.
    "scale": 150
  }
}
