﻿/** Heke's XVM Config - 11 June 2015
 * Thx to skydivers for example and font for HP circle
 * http://forum.worldoftanks.eu/index.php?/topic/465295-095-xvm-minimap-hp-config-last-pos/
 * 
 * Minimap labels. Basic HTML/CSS supported.
 */
{
	"labels": {
      // {{vehicle-class}} macro substitutions.
		"vehicleclassmacro": {
			"light": 	"<font face='vtype' size='10'>&#x69;</font>",	// &#x3A;
			"medium": 	"<font face='vtype' size='10'>&#x68;</font>",	// &#x3B;
			"heavy": 	"<font face='vtype' size='10'>&#x67;</font>",	// &#x3F;
			"td": 		"<font face='vtype' size='10'>&#x6A;</font>",	// &#x2E;
			"spg": 		"<font face='vtype' size='10'>&#x6B;</font>",	// &#x2D;
			"superh": 	"<font face='vtype' size='10'>&#x6C;</font>"
		},
        // Special symbols website
        // http://www.fileformat.info/info/unicode/char/25a0/index.htm
        // Great symbolic font by Andrey_Hard for {{vehicle-class}}:
        // http://goo.gl/d2KIj
		
		// Textfields for tanks on minimap.
		"units": {
			// Textfields switch for revealed units.
			"revealedEnabled": true,
			// Textfields switch for lost enemy units. Show last seen position.
			"lostEnemyEnabled": true,
			"format": {
				/* With dark background on the hp circle. Doesn't always work, sometimes the dark gets in front of the indicator
				"ally":           "<textformat leading='-17'><font face='dynamic' size='16' color='#101010'>{{hp-ratio=100?   |{{hp-ratio?&#536;|   }}}}</font><br><font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio=100?   |{{hp-ratio%.436a}}}}</font><span class='mm_a'>{{vehicle}}</span></textformat>",
				"teamkiller":     "<textformat leading='-17'><font face='dynamic' size='16' color='#101010'>{{hp-ratio=100?   |{{hp-ratio?&#536;|   }}}}</font><br><font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio=100?   |{{hp-ratio%.436a}}}}</font><span class='mm_t'>{{vehicle}}</span></textformat>",
				"enemy":          "<textformat leading='-17'><font face='dynamic' size='16' color='#101010'>{{hp-ratio=100?   |{{hp-ratio?&#536;|   }}}}</font><br><font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio=100?   |{{hp-ratio%.436a|   }}}}</font><span class='mm_e'>{{vehicle}}</span></textformat>",
				"squad":          "<textformat leading='-17'><font face='dynamic' size='16' color='#101010'>{{hp-ratio=100?   |{{hp-ratio?&#536;|   }}}}</font><br><font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio=100?   |{{hp-ratio%.436a}}}}</font><span class='mm_s'>{{vehicle}}</span></textformat>",
				// Own marker or spectated subject.
				"oneself":        "<textformat leading='-17'><font face='dynamic' size='16' color='#101010'>{{alive?{{hp-ratio=100?   |&#536;}}|   }}</font><br><font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio=100?   |{{hp-ratio%.436a}}}}</font></textformat>",	// <font face='dynamic' size='16' color='#333333' alpha='#A0'>&#536;</font>
				*/
				"ally":           "<font face='dynamic' size='16' color='{{c:system}}' alpha='{{a:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_a'><font color='{{c:vtype}}'>{{vehicle}}</font></span>",
				"teamkiller":     "<font face='dynamic' size='16' color='{{c:system}}' alpha='{{a:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_t'><font color='{{c:vtype}}'>{{vehicle}}</font></span>",
				"enemy":          "<font face='dynamic' size='16' color='{{c:system}}' alpha='{{a:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_e'><font color='{{c:vtype}}'>{{vehicle}}</font></span>",
				"squad":          "<font face='dynamic' size='16' color='{{c:system}}' alpha='{{a:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_s'><font color='{{c:vtype}}'>{{vehicle}}</font></span>",
				// Own marker or spectated subject.
				"oneself":        "",
				// Out of radio range ally    
				"lostally":       "<textformat leading='-13'><span class='mm_dot'> {{vehicle-class}}<br><font face='dynamic' size='16' alpha='{{a:hp-ratio}}'>{{hp-ratio%.436a|   }}</font></span><span class='mm_la'>{{vehicle}}</span></textformat>",
				// Out of radio range teamkiller
				"lostteamkiller": "<textformat leading='-13'><span class='mm_dot'> {{vehicle-class}}<br><font face='dynamic' size='16' alpha='{{a:hp-ratio}}'>{{hp-ratio%.436a|   }}</font></span><span class='mm_lt'>{{vehicle}}</span></textformat>",
				// Lost enemy units.
				"lost":           "<textformat leading='-13'><span class='mm_dot'> {{vehicle-class}}<br><font face='dynamic' size='16' alpha='{{a:hp-ratio}}'>{{hp-ratio%.436a|   }}</font></span><span class='mm_l'>{{vehicle}}</span></textformat>",	// thin space &#8201; non-breaking space &#8239;
				// Out of radio range squadman
				"lostsquad":      "<textformat leading='-13'><span class='mm_dot'> {{vehicle-class}}<br><font face='dynamic' size='16' alpha='{{a:hp-ratio}}'>{{hp-ratio%.436a|   }}</font></span><span class='mm_ls'>{{vehicle}}</span></textformat>",
				"deadally":       "<span class='mm_dot'>&#x2B;</span><span class='mm_da'></span>",
				"deadteamkiller": "<span class='mm_dot'>&#x2B;</span><span class='mm_dt'></span>",
				"deadenemy":      "<span class='mm_dot'>&#x2B;</span><span class='mm_de'></span>",
				"deadsquad":      "<span class='mm_dot'>&#x2B;</span><span class='mm_ds'>{{nick}}</span>"
			},
			// CSS style (fonts and colors option)
			"css": {
				"ally":            ".mm_a{font-family:$FieldFont; font-size:6px; color:#C8FFA6;}",
				"teamkiller":      ".mm_t{font-family:$FieldFont; font-size:6px; color:#A6F8FF;}",
				"enemy":           ".mm_e{font-family:$FieldFont; font-size:6px; color:#FCA9A4;}",
				"squad":           ".mm_s{font-family:$FieldFont; font-size:6px; color:#FFD099;}",
				"oneself":         ".mm_o{font-family:$FieldFont; font-size:8px; color:#FFFFFF;}",
				"lostally":       ".mm_la{font-family:$FieldFont; font-size:6px; color:#C8FFA6;} .mm_dot{font-family:xvmsymbol; font-size:12px; color:#B4E595;}",
				"lostteamkiller": ".mm_lt{font-family:$FieldFont; font-size:6px; color:#A6F8FF;} .mm_dot{font-family:xvmsymbol; font-size:12px; color:#00D2E5;}",
				"lost":            ".mm_l{font-family:$FieldFont; font-size:6px; color:#E97069;} .mm_dot{font-family:xvmsymbol; font-size:12px; color:#E97069;}",
				"lostsquad":      ".mm_ls{font-family:$FieldFont; font-size:6px; color:#FFD099;} .mm_dot{font-family:xvmsymbol; font-size:12px; color:#E5BB8A;}",
				"deadally":       ".mm_da{font-family:$FieldFont; font-size:6px; color:#A0A0A0;} .mm_dot{font-family:xvmsymbol; font-size:7px; color:#A0A0A0;}",
				"deadteamkiller": ".mm_dt{font-family:$FieldFont; font-size:6px; color:#A0A0A0;} .mm_dot{font-family:xvmsymbol; font-size:7px; color:#A0A0A0;}",
				"deadenemy":      ".mm_de{font-family:$FieldFont; font-size:6px; color:#A0A0A0;} .mm_dot{font-family:xvmsymbol; font-size:7px; color:#A0A0A0;}",
				"deadsquad":      ".mm_ds{font-family:$FieldFont; font-size:6px; color:#A0A0A0;} .mm_dot{font-family:xvmsymbol; font-size:7px; color:#A0A0A0;}"
				/*
				"deadally":       ".mm_da{font-family:$FieldFont; font-size:6px; color:#032900;} .mm_dot{font-family:xvmsymbol; font-size:7px; color:#032900;}",
				"deadteamkiller": ".mm_dt{font-family:$FieldFont; font-size:6px; color:#5B898C;} .mm_dot{font-family:xvmsymbol; font-size:7px; color:#043A40;}",
				"deadenemy":      ".mm_de{font-family:$FieldFont; font-size:6px; color:#4D0300;} .mm_dot{font-family:xvmsymbol; font-size:7px; color:#4D0300;}",
				"deadsquad":      ".mm_ds{font-family:$FieldFont; font-size:6px; color:#997C5C;} .mm_dot{font-family:xvmsymbol; font-size:7px; color:#663800;}"
				*/
			},
			// Fields shadow.
			"shadow": {
				"ally":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"teamkiller":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"enemy":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"squad":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"oneself":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"lostally":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"lostteamkiller":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"lost":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"lostsquad":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"deadally":
				{ "enabled": true, "color": "0x032900", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 3 },
				"deadteamkiller":
				{ "enabled": true, "color": "0x043A40", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 3 },
				"deadenemy":
				{ "enabled": true, "color": "0x4D0300", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 3 },
				"deadsquad":
				{ "enabled": true, "color": "0x663800", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 3 }
			},
			// Field offset relative to current icon (except lost - relative to enemy last seen position).
			"offset": {
				"ally":           {"x": -7.5, "y": -9.6},
				"teamkiller":     {"x": -7.5, "y": -9.6},
				"enemy":          {"x": -7.5, "y": -9.6},
				"squad":          {"x": -7.5, "y": -9.6},
				"oneself":        {"x": 0, "y": 0},
				"lostally":       {"x": -7.6, "y": -9.4},
				"lostteamkiller": {"x": -7.6, "y": -9.4},
				"lost":           {"x": -7.6, "y": -9.4},
				"lostsquad":      {"x": -7.6, "y": -9.4},
				"deadally":       {"x": -5, "y": -5.7},
				"deadteamkiller": {"x": -5, "y": -5.7},
				"deadenemy":      {"x": -5, "y": -5.7},
				"deadsquad":      {"x": -5, "y": -5.7}
			},
			"alpha" : {
				"ally": 100,
				"teamkiller": 100,
				"enemy": 100,
				"squad": 100,
				"oneself": 100,
				"lostally": 80,
				"lostteamkiller": 80,
				"lost": 80,
				"lostsquad": 80,
				"deadally": 70,
				"deadteamkiller": 70,
				"deadenemy": 70,
				"deadsquad": 70
			}
		},
		// Textfield for map side size. 1000m, 700m, 600m.
		"mapSize": {
			"enabled": true,
			"format": "<b>{{cellsize}}0 m</b>",
			"css": "font-size:6px; font-family:$FieldFont; color:#959569;",	//"#89896C;",
			"alpha": 100,
			"offsetX": 1,
			"offsetY": 0,
			"shadow": {
				"enabled": true,
				"color": "0x000000",
				"distance": 0,
				"angle": 0,
				"alpha": 80,
				"blur": 3,
				"strength": 3
			},
			// Decrease sizes in case of map image weird shrinking while map resize.
			// Increase sizes in case of field being partially cut off.
			// -------------------------------------------------------------------------------------
			"width": 100,
			"height": 30
		}
    }
}