﻿/**
 * Textfield for map side size. 1000m, 700m, 600m.
 */
{
	"mapSize": {
		"enabled": true,
		"format": "<font face='$FieldFont' size='7' color='#959569'><b>{{cellsize}}0 m</b></font>",
		"alpha": 100,
		"offsetX": 1,
		"offsetY": 0,
		"shadow": {
			"enabled": true,
			"color": "0x000000",
			"distance": 0,
			"angle": 0,
			"alpha": 80,
			"blur": 3,
			"strength": 3
		},
		// Decrease sizes in case of map image weird shrinking while map resize.
		// Increase sizes in case of field being partially cut off.
		// -------------------------------------------------------------------------------------
		"width": 100,
		"height": 30
	}
}
