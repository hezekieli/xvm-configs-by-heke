﻿/** Heke's XVM Config - 11 June 2015
 * Thx to Bones for example HP-bars
 * http://worldof-tanks.com/0-9-2-xvm-config-by-bones/
 *
 * Parameters of the Players Panels ("ears").
 */
{
  // Enemy spotted status marker format for substitutions in extra fields.
  "enemySpottedMarker": {
    // Opacity percentage of spotted markers in the panels. 0 - transparent (disabled) ... 100 - opaque.
    "alpha": 100,
    // x position.
    "x": -50,
    // y position.
    "y": 6,
    // true - x position is binded to vehicle icon, false - binded to edge of the screen.
    "bindToIcon": true,
    // enemy spotted status marker format.
    "format": "{{spotted}}",
    // shadow (see below).
    "shadow": {}
  },
  // Parameters of the Players Panels ("ears").
  "playersPanel": {
    // Opacity percentage of the panels. 0 - transparent, 100 - opaque.
    "alpha": 60,
    // Opacity percentage of icons in the panels. 0 - transparent ... 100 - opaque.
    "iconAlpha": 100,
    // true - disable background of the selected player.
    "removeSelectedBackground": false,
    // true - Remove the Players Panel mode switcher (buttons for changing size).
    "removePanelsModeSwitcher": false,
    // Start panels mode. Possible values: "none", "short", "medium", "medium2", "large".
    "startMode": "medium2",
    // Alternative panels mode. Possible values: null, "none", "short", "medium", "medium2", "large".
    "altMode": "medium",
    // Display options for Team/Clan logos (see battleLoading.xc).
    "clanIcon": {
      "show": true,
      "x": -6,
      "y": 6,
      "xr": -6,
      "yr": 6,
      "w": 16,
      "h": 16,
      "alpha": 100
    },
    // Options for the "none" panels - empty panels.
    "none": {
      // false - disable 
      "enabled": true,
      // Layout ("vertical" or "horizontal")
      "layout": "vertical",
      // Extra fields.
      "extraFields": {
        "leftPanel": {
          "x": 0, // from left side of screen
          "y": 65,
          "width": 350,
          "height": 25,
          // Set of formats for left panel
          // example:
          // "format": [
          //   // simple format (just a text)
          //   "{{nick}}",
          //   "<img src='xvm://res/img/panel-bg-l-{{alive|dead}}.png' width='318' height='28'>",
          //   // extended format
          //   { "x": 20, "y": 10, "border": 1, "borderColor": "0xFFFFFF", "format": "{{nick}}" },
          //   { "x": 200, "src": "xvm://res/contour/{{vehiclename}}.png" }
          // ]
          //
          // types of formats available for extended format:
          //   - MovieClip (for loading image)
          //   - TextField (for writing text and creating rectangles)
          // if "src" field is present, MovieClip format will be used
          // if "src" field is absent, TextField format will be used
          //
          // fields available for extended format:
          //   "src" - resource path ("xvm://res/contour/{{vehiclename}}.png")
          //   "format" - text format (macros allowed)
          //
          // fields available for both MovieClip and TextField formats:
          //   "x" - x position (macros allowed)
          //   "y" - y position (macros allowed)
          //   "w" - width (macros allowed)
          //   "h" - height (macros allowed)
          //   "alpha" - transparency in percents (0..100) (macros allowed)
          //   "rotation" - rotation in degrees (0..360) (macros allowed)
          //   "align" - horizontal alignment ("left", "center", "right")
          //      for left panel default value is "left"
          //      for right panel default value is "right"
          //
          // fields available for TextField format only:
          //   "valign" - vertical alignment ("top", "center", "bottom")
          //      default value is "top"
          //   "borderColor" - if set, draw border with specified color (macros allowed)
          //   "bgColor" - if set, draw background with specified color (macros allowed)
          //   "antiAliasType" - anti aliasing mode ("advanced" or "normal")
          //   "shadow": {
          //     "distance" (in pixels)
          //     "angle"    (0.0 .. 360.0)
          //     "color"    "0xXXXXXX"
          //     "alpha"    (0.0 .. 1.0)
          //     "blur"     (0.0 .. 255.0)
          //     "strength" (0.0 .. 255.0)
          //    }
          //
          // * all fields are optional
          //
          "formats": []
        },
        "rightPanel": {
          "x": 0, // from right side of screen
          "y": 65,
          "width": 350,
          "height": 25,
          // Set of formats for right panel (extended format supported, see above)
          "formats": []
        }
      }
    },
    // Options for the "short" panels - panels with frags and vehicle icon.
    "short": {
      // false - disable (?????????)
      "enabled": true,
      // Width of the column, 0-250. Default is 0.
      "width": 0,
      // true - disable platoon icons
      "removeSquadIcon": false,
      // Display format for frags (macros allowed, see readme-en.txt).
      "fragsFormatLeft": "{{frags}}",
      "fragsFormatRight": "{{frags}}",
      // Extra fields. Each field have size 350x25. Fields are placed one above the other.
      // Set of formats for left panel (extended format supported, see above)
      "extraFieldsLeft": [],
      // Set of formats for right panel (extended format supported, see above)
      "extraFieldsRight": [
        // enemy spotted status marker (see above).
        ${"enemySpottedMarker"}
      ]
    },
    // Options for the "medium" panels - the first of the medium panels.
    "medium": {
      // false - disable
      "enabled": true,
      // Width of the player's name column, 0-250. Default is 46.
      "width": 50,
      // true - disable platoon icons
      "removeSquadIcon": false,
      // Display format for the left panel (macros allowed, see readme-en.txt).
      "formatLeft": "<font color='{{c:wn8}}' alpha='{{alive?#FF|#80}}'>{{name%.15s~..}}</font><font color='#95957B' alpha='{{alive?#FF|#80}}'>{{clan}}</font>",
      // Display format for the right panel (macros allowed, see readme-en.txt).
      "formatRight": "<font color='{{c:wn8}}' alpha='{{alive?#FF|#80}}'>{{name%.15s~..}}</font><font color='#95957B' alpha='{{alive?#FF|#80}}'>{{clan}}</font>",
      // Display format for frags (macros allowed, see readme-en.txt).
      "fragsFormatLeft": "{{frags}}",
      "fragsFormatRight": "{{frags}}",
      // Extra fields. Each field have size 350x25. Fields are placed one above the other.
      // Set of formats for left panel (extended format supported, see above)
	"extraFieldsLeft": [
		{ "x": 32, "y": 19, "valign": "center", "w": 65, "h": 4, "bgColor": "0x000000", "alpha": "{{alive?100|0}}" },
		{ "x": 33, "y": 20, "valign": "center", "w": "{{hp-ratio:63|63}}", "h": 2, "bgColor": "{{c:hp-ratio|0x808080}}", "alpha": "{{alive?100|0}}" }
	],
	"extraFieldsRight": [
		{ "x": 32, "y": 19, "valign": "center", "w": 65, "h": 4, "bgColor": "0x000000", "alpha": "{{alive?100|0}}" },
		{ "x": 33, "y": 20, "valign": "center", "w": "{{hp-ratio:63|63}}", "h": 2, "bgColor": "{{c:hp-ratio|0x808080}}", "alpha": "{{alive?100|0}}" },
        // enemy spotted status marker (see above).
        ${"enemySpottedMarker"}
      ]
    },
    // Options for the "medium2" panels - the second of the medium panels.
    "medium2": {
      // false - disable
      "enabled": true,
      // Width of the vehicle name column, 0-250. Default is 65.
      "width": 65,
      // true - disable platoon icons
      "removeSquadIcon": false,
      // Display format for the left panel (macros allowed, see readme-en.txt).
      "formatLeft": "<font face='$FieldFont' size='12' color='{{c:vtype}}' alpha='{{alive?#FF|#80}}'>{{vehicle%.15s~..}}</font>",
      // Display format for the right panel (macros allowed, see readme-en.txt).
      "formatRight": "<font face='$FieldFont' size='12' color='{{c:vtype}}' alpha='{{alive?#FF|#80}}'>{{vehicle%.15s~..}}</font>",
      // Display format for frags (macros allowed, see readme-en.txt).
      "fragsFormatLeft": "{{frags}}",
      "fragsFormatRight": "{{frags}}",
      // Extra fields. Each field have size 350x25. Fields are placed one above the other.
      // Set of formats for left panel (extended format supported, see above)
	"extraFieldsLeft": [
		{ "x": 54, "y": 19, "valign": "center", "w": 65, "h": 4, "bgColor": "0x000000", "alpha": "{{alive?100|0}}" },
		{ "x": 55, "y": 20, "valign": "center", "w": "{{hp-ratio:63|63}}", "h": 2, "bgColor": "{{c:hp-ratio|0x808080}}", "alpha": "{{alive?100|0}}" }
	],
	"extraFieldsRight": [
		{ "x": 54, "y": 19, "valign": "center", "w": 65, "h": 4, "bgColor": "0x000000", "alpha": "{{alive?100|0}}" },
		{ "x": 55, "y": 20, "valign": "center", "w": "{{hp-ratio:63|63}}", "h": 2, "bgColor": "{{c:hp-ratio|0x808080}}", "alpha": "{{alive?100|0}}" },
        // enemy spotted status marker (see above).
        ${"enemySpottedMarker"}
      ]
    },
    // Options for the "large" panels - the widest panels.
    "large": {
      // false - disable
      "enabled": true,
      // Width of the player's name column, 0-250. Default is 170.
      "width": 150,
      // true - disable platoon icons
      "removeSquadIcon": false,
      // Display format for player nickname (macros allowed, see readme-en.txt).      
	  "nickFormatLeft": "<img src='xvm://res/icons/xvm/xvm-user-{{xvm-user}}.png'> <font face='mono' width='20' color='{{c:kb}}' alpha='{{alive?#FF|#80}}'>{{kb%2d~k|--k}}</font>|<font face='mono' width='30' color='{{c:wn8}}' alpha='{{alive?#FF|#80}}'>{{wn8%4d|----}}</font>|<font face='mono' width='24' color='{{c:winrate}}' alpha='{{alive?#FF|#80}}'>{{winrate%d~%|--%}}</font>|<font alpha='{{alive?#CC|#80}}'>{{name%.15s~..}}</font> <font color='#95957B' alpha='{{alive?#F0|#80}}'>{{clan}}</font> ",
      
	  "nickFormatRight": " <font alpha='{{alive?#CC|#80}}'>{{name%.15s~..}}</font><font color='#95957B' alpha='{{alive?#F0|#80}}'>{{clan}}</font>|<font face='mono' width='24' color='{{c:rating}}' alpha='{{alive?#FF|#80}}'>{{rating%d~%|--%}}</font>|<font face='mono' width='30' color='{{c:wn8}}' alpha='{{alive?#FF|#80}}'>{{wn8%4d|----}}</font>|<font face='mono' width='20' color='{{c:kb}}' alpha='{{alive?#FF|#80}}'>{{kb%2d~k|--k}}</font> <img src='xvm://res/icons/xvm/xvm-user-{{xvm-user}}.png'>",
      // Display format for vehicle name (macros allowed, see readme-en.txt).
      "vehicleFormatLeft": "<font color='{{c:vtype}}' alpha='{{alive?#FF|#80}}'>{{vehicle}}</font>",
      "vehicleFormatRight": "<font color='{{c:vtype}}' alpha='{{alive?#FF|#80}}'>{{vehicle}}</font>",
      // Display format for frags (macros allowed, see readme-en.txt).
      "fragsFormatLeft": "{{frags}}",
      "fragsFormatRight": "{{frags}}",
      // Extra fields. Each field have size 350x25. Fields are placed one above the other.
      // Set of formats for left panel (extended format supported, see above)
      "extraFieldsLeft": [
			{ "x": 134, "y": 19, "valign": "center", "w": 65, "h": 4, "bgColor": "0x000000", "alpha": "{{alive?100|0}}" },
			{ "x": 135, "y": 20, "valign": "center", "w": "{{hp-ratio:63|63}}", "h": 2, "bgColor": "{{c:hp-ratio|0x808080}}", "alpha": "{{alive?100|0}}" }
		],
      // Set of formats for right panel (extended format supported, see above)
      "extraFieldsRight": [
			{ "x": 134, "y": 19, "valign": "center", "w": 65, "h": 4, "bgColor": "0x000000", "alpha": "{{alive?100|0}}" },
			{ "x": 135, "y": 20, "valign": "center", "w": "{{hp-ratio:63|63}}", "h": 2, "bgColor": "{{c:hp-ratio|0x808080}}", "alpha": "{{alive?100|0}}" },
        // enemy spotted status marker (see above).
        ${"enemySpottedMarker"}
      ]
    }
  }
}
