﻿/** Heke's XVM Config - 11 June 2015
 * Color settings.
 */
{
  // Color values for substitutions.
  "def": {
	"al": "0x90D033", // ally       / "0x33E0E0"  "0x99FF00"
    "sq": "0xE0C066", // squadman   / "0x33CCFF"  "0x33EE99",
    "tk": "0xFFE000", // teamKiller /  "0x0099FF",
    "en": "0xE02020", // enemy      /  "0xC066CC", "0xFF1100"
    "pl": "0xFFA000", // player     /  "0x33EE99",
    // Dynamic color by various statistical parameters.
    "colorRating": {
      "bad":          "0xC00066",   // terrible-WoTLabs Bad   / 
      "below_avg":    "0x993399",   // very bad-WoTLabs Below Average   / 
      "average":      "0x9966FF",   // bad-WoTLabs Average        / 
      "above_avg":    "0x3399FF",   // below average-WoTLabs Above Average      
      "intermediate": "0x00E0E0",   // normal/average-WoTLabs Intermediate     / 
      "adequate":     "0x00E0E0",   // good-WoTLabs Adequate       / 
      "good":         "0x00E0E0",   // very good-WoTLabs Good  / 
      "very_good":    "0x00E0E0",   // very very good-WoTLabs Very Good  /       
      "great":        "0x00E0E0",   // great-WoTLabs Great
      "unicum":       "0x00E0E0",   // unique/unicum-WoTLabs Unicum     / 
      "super_unicum": "0x00E0E0"    // very unique/super unicum-WoTLabs Super Unicum      
    },
    // Dynamic color by remaining health points. Used in HP bars and circles
    "colorHP": {
      "very_low":       "0xBB2020",   // very low       /    993333
      "low":            "0xBB7020",   // low            /          996633
      "average":        "0xBBAA33",   // average        /         999966
      "above_average":  "0x40B020",    // above-average  /   00cc99 66A333
	  "full":    		"0x336633"    // full  /   00cc99 66A333
    },
	// Darker colors for kilobattles and winrate to distract less
	"colorDarkRating": {
		"bad":			"0x806060", 
		"below_avg":	"0xA08980", 
    	"average":		"0xB0B0A0",
		"above_avg":	"0xC9E0C0",
		"good":			"0xE0FFE0",
		"great":	 	"0xF0FFF0",
		"unicum": 		"0xFFFFFF"
	}
  },
  "colors": {
    // System colors.
    // ????????? ?????.
    "system": {
      // Format: object_state
      // Object:      ally, squadman, teamKiller, enemy
      // State:       alive, dead, blowedup
      // ----
      "ally_alive":          ${"def.al"},
      "ally_dead":           "0x009900",
      "ally_blowedup":       "0x007700",
      "squadman_alive":      ${"def.sq"},
      "squadman_dead":       "0xCA7000",
      "squadman_blowedup":   "0xA45A00",
      "teamKiller_alive":    ${"def.tk"},
      "teamKiller_dead":     "0x097783",
      "teamKiller_blowedup": "0x096A75",
      "enemy_alive":         ${"def.en"},
      "enemy_dead":          "0x840500",
      "enemy_blowedup":      "0x5A0401",
      "ally_base":           ${"def.al"},
      "enemy_base":          ${"def.en"}
    },
    // Dynamic color by damage kind.
    "dmg_kind": {
      "shot": "0xFFAA55",            // shot / ?????????
      "fire": "0xFF6655",            // fire / ?????
      "ramming": "0x998855",         // ramming / ?????
      "world_collision": "0x228855", // world collision / ???????????? ? ?????????, ???????
      "death_zone": "0xCCCCCC",      // TODO: value, description
      "drowning": "0xCCCCCC",        // TODO: value, description
      "other": "0xCCCCCC"            // other / ??????
    },
    // Dynamic color by vehicle type.
    "vtype": {
      "LT":  "0xF9F080",	// "0xF9F080",	//"0xA2FF9A",
      "MT":  "0x8FD48A",	// "0x8FD48A",	//"0xFFF198",
      "HT":  "0xF6B990",	// "0xF6B990",	//"0xFFC6AC",
      "SPG": "0xBC8888",	// "0xBC7373",	//"0xEFAEFF",
      "TD":  "0x99B0D8",	// "0x7398D8",	//"0xA0CFFF",
      "premium": "0xFFCC66",
      "usePremiumColor": false
    },
    // Dynamic color by spotted status
    "spotted": {
      "neverSeen": "0x000000",
      "lost": "0x999999",
      "revealed": "0x00DE00",
      "dead": "0x000000",
      "neverSeen_arty": "0x000000",
      "lost_arty": "0x999999",
      "revealed_arty": "0xDE0000",
      "dead_arty": "0x000000"
    },
    // Color settings for damage.
    "damage": {
      // Format: src_dst_type.
      // Src:  ally, squadman, enemy, unknown, player.
      // Dst:  ally, squadman, allytk, enemytk, enemy.
      // Type: hit, kill, blowup.
      // ----
      "ally_ally_hit":              ${"def.tk"},
      "ally_ally_kill":             ${"def.tk"},
      "ally_ally_blowup":           ${"def.tk"},
      "ally_squadman_hit":          ${"def.tk"},
      "ally_squadman_kill":         ${"def.tk"},
      "ally_squadman_blowup":       ${"def.tk"},
      "ally_enemy_hit":             ${"def.al"},
      "ally_enemy_kill":            ${"def.al"},
      "ally_enemy_blowup":          ${"def.al"},
      "ally_allytk_hit":            ${"def.tk"},
      "ally_allytk_kill":           ${"def.tk"},
      "ally_allytk_blowup":         ${"def.tk"},
      "ally_enemytk_hit":           ${"def.al"},
      "ally_enemytk_kill":          ${"def.al"},
      "ally_enemytk_blowup":        ${"def.al"},
      "enemy_ally_hit":             ${"def.en"},
      "enemy_ally_kill":            ${"def.en"},
      "enemy_ally_blowup":          ${"def.en"},
      "enemy_squadman_hit":         ${"def.en"},
      "enemy_squadman_kill":        ${"def.en"},
      "enemy_squadman_blowup":      ${"def.en"},
      "enemy_enemy_hit":            ${"def.en"},
      "enemy_enemy_kill":           ${"def.en"},
      "enemy_enemy_blowup":         ${"def.en"},
      "enemy_allytk_hit":           ${"def.en"},
      "enemy_allytk_kill":          ${"def.en"},
      "enemy_allytk_blowup":        ${"def.en"},
      "enemy_enemytk_hit":          ${"def.en"},
      "enemy_enemytk_kill":         ${"def.en"},
      "enemy_enemytk_blowup":       ${"def.en"},
      "unknown_ally_hit":           ${"def.al"},
      "unknown_ally_kill":          ${"def.al"},
      "unknown_ally_blowup":        ${"def.al"},
      "unknown_squadman_hit":       ${"def.sq"},
      "unknown_squadman_kill":      ${"def.sq"},
      "unknown_squadman_blowup":    ${"def.sq"},
      "unknown_enemy_hit":          ${"def.en"},
      "unknown_enemy_kill":         ${"def.en"},
      "unknown_enemy_blowup":       ${"def.en"},
      "unknown_allytk_hit":         ${"def.tk"},
      "unknown_allytk_kill":        ${"def.tk"},
      "unknown_allytk_blowup":      ${"def.tk"},
      "unknown_enemytk_hit":        ${"def.en"},
      "unknown_enemytk_kill":       ${"def.en"},
      "unknown_enemytk_blowup":     ${"def.en"},
      "squadman_ally_hit":          ${"def.sq"},
      "squadman_ally_kill":         ${"def.sq"},
      "squadman_ally_blowup":       ${"def.sq"},
      "squadman_squadman_hit":      ${"def.sq"},
      "squadman_squadman_kill":     ${"def.sq"},
      "squadman_squadman_blowup":   ${"def.sq"},
      "squadman_enemy_hit":         ${"def.sq"},
      "squadman_enemy_kill":        ${"def.sq"},
      "squadman_enemy_blowup":      ${"def.sq"},
      "squadman_allytk_hit":        ${"def.sq"},
      "squadman_allytk_kill":       ${"def.sq"},
      "squadman_allytk_blowup":     ${"def.sq"},
      "squadman_enemytk_hit":       ${"def.sq"},
      "squadman_enemytk_kill":      ${"def.sq"},
      "squadman_enemytk_blowup":    ${"def.sq"},
      "player_ally_hit":            ${"def.pl"},
      "player_ally_kill":           ${"def.pl"},
      "player_ally_blowup":         ${"def.pl"},
      "player_squadman_hit":        ${"def.pl"},
      "player_squadman_kill":       ${"def.pl"},
      "player_squadman_blowup":     ${"def.pl"},
      "player_enemy_hit":           ${"def.pl"},
      "player_enemy_kill":          ${"def.pl"},
      "player_enemy_blowup":        ${"def.pl"},
      "player_allytk_hit":          ${"def.pl"},
      "player_allytk_kill":         ${"def.pl"},
      "player_allytk_blowup":       ${"def.pl"},
      "player_enemytk_hit":         ${"def.pl"},
      "player_enemytk_kill":        ${"def.pl"},
      "player_enemytk_blowup":      ${"def.pl"}
    },
    // Values below should be from smaller to larger.
    // ----
    // Dynamic color by remaining absolute health.
    "hp": [
      { "value": 201,  "color": ${"def.colorHP.very_low"     } },      // 201
      { "value": 401,  "color": ${"def.colorHP.low"          } },      // 401
      { "value": 1001, "color": ${"def.colorHP.average"      } },      // 1001
      { "value": 9999, "color": ${"def.colorHP.above_average"} }       // 
    ],
    // Dynamic color by remaining health percent.
    "hp_ratio": [
		{ "value": 25,  "color": ${"def.colorHP.very_low"     	} },       // 10 
		{ "value": 50,  "color": ${"def.colorHP.low"          	} },       // 25 
		{ "value": 75,  "color": ${"def.colorHP.average"      	} },       // 50 
		{ "value": 99, 	"color": ${"def.colorHP.above_average"	} },        //
		{ "value": 101, "color": ${"def.colorHP.full"			} }        // 	  
    ],
    // Dynamic color for XVM Scale
    // http://www.koreanrandom.com/forum/topic/2625-/
    "x": [
      { "value": 16.5,  "color": ${"def.colorRating.below_avg"      } },   // 00   - 16.5 - very bad   (20% of players)
      { "value": 33.5,  "color": ${"def.colorRating.average"        } },   // 16.5 - 33.5 - bad        (better then 20% of players)
      { "value": 52.5,  "color": ${"def.colorRating.intermediate"   } },   // 33.5 - 52.5 - normal     (better then 60% of players)
      { "value": 75.5,  "color": ${"def.colorRating.adequate"       } },   // 52.5 - 75.5 - good       (better then 90% of players)
      { "value": 92.5,  "color": ${"def.colorRating.great"          } },   // 75.5 - 92.5 - very good  (better then 99% of players)
      { "value": 999, "color": ${"def.colorRating.unicum"         } }    // 92.5 - XX   - unique     (better then 99.9% of players)
    ],
    // Dynamic color by efficiency
    "eff": [
      { "value": 610,  "color": ${"def.colorRating.below_avg"     } },  //    0 - 629  - very bad
      { "value": 865,  "color": ${"def.colorRating.average"       } },  //  630 - 859  - bad
      { "value": 1165, "color": ${"def.colorRating.intermediate"  } },  //  860 - 1139 - normal
      { "value": 1515, "color": ${"def.colorRating.adequate"      } },  // 1140 - 1459 - good
      { "value": 1835, "color": ${"def.colorRating.great"         } },  // 1460 - 1734 - very good
      { "value": 9999, "color": ${"def.colorRating.unicum"        } }   // 1735 - *    - unique
    ],
    // Dynamic color by WN6 rating
    "wn6": [
      { "value": 410,  "color": ${"def.colorRating.below_avg"    } },  //    0 - 499  - very bad
      { "value": 795,  "color": ${"def.colorRating.average"      } },  //  500 - 699  - bad
      { "value": 1000,  "color": ${"def.colorRating.above_avg"    } },  //  700 - 899  - below avg
      { "value": 1185, "color": ${"def.colorRating.intermediate" } },  //  900 - 1099 - normal
      { "value": 1385, "color": ${"def.colorRating.adequate"     } },  // 1100 - 1349 - good
      { "value": 1585, "color": ${"def.colorRating.good"         } },  // 1350 - 1499 - very good
      { "value": 1755, "color": ${"def.colorRating.very_good"    } },  //
      { "value": 1925, "color": ${"def.colorRating.great"        } },  // 1500 - 1699 - great
      { "value": 2200, "color": ${"def.colorRating.unicum"       } },  // 1700 - 1999 - unicum
      { "value": 9999, "color": ${"def.colorRating.super_unicum" } }   // 2000 - *    - super unicum
    ],
    // Dynamic color by WN8 rating
    "wn8": [
      { "value": 300,  "color": ${"def.colorRating.bad"          } },  //    
      { "value": 600,  "color": ${"def.colorRating.below_avg"    } },  //    
      { "value": 900,  "color": ${"def.colorRating.average"      } },  //  
      { "value": 1200,  "color": ${"def.colorRating.above_avg"    } },  //  
      { "value": 1500, "color": ${"def.colorRating.intermediate"   } },  //  
      { "value": 1600, "color": ${"def.colorRating.adequate"     } },  // 
      { "value": 1800, "color": ${"def.colorRating.good"         } },  // 
      { "value": 2000, "color": ${"def.colorRating.very_good"    } },  // 
      { "value": 2450, "color": ${"def.colorRating.great"        } },  // 
      { "value": 2900, "color": ${"def.colorRating.unicum"       } },  // 
      { "value": 9999, "color": ${"def.colorRating.super_unicum" } }   // 
    ],    
    // Dynamic color by WG rating
    // TODO: update values
    "wgr": [
      { "value": 2405,  "color": ${"def.colorRating.below_avg" } },  // very bad   (20% of players)
      { "value": 4250,  "color": ${"def.colorRating.average"      } },  // bad        (better then 20% of players)
      { "value": 6350,  "color": ${"def.colorRating.intermediate"   } },  // normal     (better then 60% of players)
      { "value": 8550,  "color": ${"def.colorRating.adequate"     } },  // good       (better then 90% of players)
      { "value": 9960,  "color": ${"def.colorRating.great"} },  // very good  (better then 99% of players)
      { "value": 99999, "color": ${"def.colorRating.unicum"   } }   // unique     (better then 99.9% of players)
    ],
    // Dynamic color for win chance
    // ???????????? ???? ??? ????? ?? ??????
    "winChance": [
      { "value": 24.5, "color": ${"def.colorRating.bad" } },
      { "value": 39.5, "color": ${"def.colorRating.average"      } },
      { "value": 47.5, "color": ${"def.colorRating.above_avg" } },
      { "value": 49.5, "color": ${"def.colorRating.intermediate"   } },
      { "value": 51.5, "color": ${"def.colorRating.adequate"     } },
      { "value": 59.5, "color": ${"def.colorRating.good"} },
      { "value": 74.5, "color": ${"def.colorRating.very_good"} },
      { "value": 89.5, "color": ${"def.colorRating.great"} },
      { "value": 101,  "color": ${"def.colorRating.unicum"   } }
    ],
    // Dynamic color by win percent
    "winrate": [
      { "value": 45.5,  "color": ${"def.colorDarkRating.bad" 		} },	//  
      { "value": 48.5,  "color": ${"def.colorDarkRating.below_avg"	} },	//  
      { "value": 50.5, 	"color": ${"def.colorDarkRating.average"    } },	// 
      { "value": 52.5, 	"color": ${"def.colorDarkRating.above_avg" 	} },	//  
      { "value": 55.5,	"color": ${"def.colorDarkRating.good"		} },	// 
      { "value": 59.5,	"color": ${"def.colorDarkRating.great"    	} },	// 
      { "value": 100.5,	"color": ${"def.colorDarkRating.unicum"   	} }		//  
    ],
    // Dynamic color by kilo-battles
    "kb": [
      { "value": 2,		"color": ${"def.colorDarkRating.bad" 		} },   //    0 - 1
      { "value": 4,		"color": ${"def.colorDarkRating.below_avg"	} },   //  2 - 3
      { "value": 7,		"color": ${"def.colorDarkRating.average"    } },   //  4 - 6
	  { "value": 11,	"color": ${"def.colorDarkRating.above_avg" 	} },	//  7 - 10
      { "value": 16, 	"color": ${"def.colorDarkRating.good"		} },	//  11 - 15
      { "value": 21, 	"color": ${"def.colorDarkRating.great"    	} },	// 16 - 20
      { "value": 999, 	"color": ${"def.colorDarkRating.unicum"   	} }		// 21 - *
    ],
    // Dynamic color by average level of player tanks
    "avglvl": [
      { "value": 2,  "color": "0xAA4444" },
      { "value": 3,  "color": "0xBB8833" },
      { "value": 5,  "color": "0x558855" },
      { "value": 7,  "color": "0x66BB88" },
      { "value": 9,  "color": "0x337788" },
      { "value": 11, "color": "0x993399" }
    ],
    // Dynamic color by battles on current tank
    "t_battles": [
      { "value": 50,    "color": "0xAA4444" }, //    0 - 49
      { "value": 100,   "color": "0xBB8833" }, //   50 - 99
      { "value": 250,   "color": "0x558855" }, //  100 - 249
      { "value": 500,   "color": "0x66BB88" }, //  250 - 499
      { "value": 1000,  "color": "0x337788" }, //  500 - 899
      { "value": 99999, "color": "0x993399" }  // 1000 - *
    ],
    // Dynamic color by average damage on current tank
    "tdb": [
      { "value": 500,  "color": ${"def.colorRating.below_avg" } },
      { "value": 1000, "color": ${"def.colorRating.intermediate"   } },
      { "value": 1800, "color": ${"def.colorRating.adequate"     } },
      { "value": 2500, "color": ${"def.colorRating.great"    } },
      { "value": 3000, "color": ${"def.colorRating.unicum"   } }
    ],
    // Dynamic color by average damage efficiency on current tank  
    "tdv": [
      { "value": 0.6,  "color": ${"def.colorRating.below_avg" } },
      { "value": 0.8,  "color": ${"def.colorRating.average"      } },
      { "value": 1.0,  "color": ${"def.colorRating.intermediate"   } },
      { "value": 1.3,  "color": ${"def.colorRating.adequate"     } },
      { "value": 2.0,  "color": ${"def.colorRating.great"    } },
      { "value": 15,   "color": ${"def.colorRating.unicum"   } }
    ],
    // Dynamic color by average frags per battle on current tank 
    "tfb": [
      { "value": 0.6,  "color": ${"def.colorRating.below_avg" } },
      { "value": 0.8,  "color": ${"def.colorRating.average"      } },
      { "value": 1.0,  "color": ${"def.colorRating.intermediate"   } },
      { "value": 1.3,  "color": ${"def.colorRating.adequate"     } },
      { "value": 2.0,  "color": ${"def.colorRating.great"    } },
      { "value": 15,   "color": ${"def.colorRating.unicum"   } }
    ],
    // Dynamic color by average number of spotted enemies per battle on current tank
    // ???????????? ???? ?? ???????? ?????????? ??????????? ?????? ?? ??? ?? ??????? ?????
    "tsb": [
      { "value": 0.6,  "color": ${"def.colorRating.below_avg" } },
      { "value": 0.8,  "color": ${"def.colorRating.average"      } },
      { "value": 1.0,  "color": ${"def.colorRating.intermediate"   } },
      { "value": 1.3,  "color": ${"def.colorRating.adequate"     } },
      { "value": 2.0,  "color": ${"def.colorRating.great"} },
      { "value": 15,   "color": ${"def.colorRating.unicum"   } }
    ],
    // Dynamic color by WN8 effective damage
    // ???????????? ???? ?? ???????????? ????? ?? WN8
    "wn8effd": [
      { "value": 0.6,  "color": ${"def.colorRating.below_avg" } },
      { "value": 0.8,  "color": ${"def.colorRating.average"      } },
      { "value": 1.0,  "color": ${"def.colorRating.intermediate"   } },
      { "value": 1.3,  "color": ${"def.colorRating.adequate"     } },
      { "value": 2.0,  "color": ${"def.colorRating.great"} },
      { "value": 15,   "color": ${"def.colorRating.unicum"   } }
    ],
    // Dynamic color by damage rating (percents for marks on gun)
    // ???????????? ???? ?? ???????? ????? (??????? ??? ??????? ?? ??????)
    "damageRating": [
      { "value": 20,    "color": ${"def.colorRating.below_avg" } },  // 20% of players
      { "value": 60,    "color": ${"def.colorRating.average"      } },  // better then 20% of players
      { "value": 90,    "color": ${"def.colorRating.intermediate"   } },  // better then 60% of players
      { "value": 99,    "color": ${"def.colorRating.adequate"     } },  // better then 90% of players
      { "value": 99.9,  "color": ${"def.colorRating.great"} },  // better then 99% of players
      { "value": 101,   "color": ${"def.colorRating.unicum"   } }   // better then 99.9% of players
    ],
    // TODO:values
    // Dynamic color by hit ratio (percents of hits)
    // ???????????? ???? ?? ???????? ?????????
    "hitsRatio": [
      { "value": 47.5,  "color": ${"def.colorRating.below_avg" } },
      { "value": 60.5,  "color": ${"def.colorRating.average"      } },
      { "value": 68.5,  "color": ${"def.colorRating.intermediate"   } },
      { "value": 74.5,  "color": ${"def.colorRating.adequate"     } },
      { "value": 78.5,  "color": ${"def.colorRating.great"} },
      { "value": 101,   "color": ${"def.colorRating.unicum"   } }
    ]
  }
}
