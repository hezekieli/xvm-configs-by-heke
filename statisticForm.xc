﻿/** Heke's XVM Config - 11 June 2015
 * Parameters of the Battle Statistics form.
 */
{
  "statisticForm": {
    // true - Enable display of battle tier.
    "showBattleTier": true,
    // true - Disable Platoon icons.
    "removeSquadIcon": false,
    // Display options for Team/Clan logos (see battleLoading.xc).
    "clanIcon": {
      "show": false,
      "x": 0,
      "y": 6,
      "xr": 0,
      "yr": 6,
      "w": 16,
      "h": 16,
      "alpha": 90
    },
    // Display format for the left panel (macros allowed, see readme-en.txt).
    "formatLeftNick": "<img src='xvm://res/icons/flags/{{flag|default}}.png' width='16' height='13' alt='{{flag}}'> <img src='xvm://res/icons/xvm/xvm-user-{{xvm-user|none}}.png'> <font alpha='{{alive?#FF|#80}}'>{{name%.16s~..}}</font> <font color='#95957B' alpha='{{alive?#A0|#60}}'>{{clan}}</font>",
    // Display format for the right panel (macros allowed, see readme-en.txt).
    "formatRightNick": "<font color='#95957B' alpha='{{alive?#A0|#60}}'>{{clan}}</font> <font alpha='{{alive?#FF|#80}}'>{{name%.16s~..}}</font> <img src='xvm://res/icons/xvm/xvm-user-{{xvm-user}}.png'> <img src='xvm://res/icons/flags/{{flag|default}}.png' width='16' height='13'>",
    // Display format for the left panel (macros allowed, see readme-en.txt).
    "formatLeftVehicle": "<font color='{{c:vtype}}' alpha='{{alive?#FF|#60}}'>{{vehicle}}</font>|<font face='mono' width='20' color='{{c:kb}}'>{{kb%2.d~k|--k}}</font>|<font face='mono' width='30' color='{{c:wn8}}'>{{wn8%4.d|----}}</font>|<font face='mono' width='24' color='{{c:rating}}'>{{rating%2.d~%|--%}}</font>",
    // Display format for the right panel (macros allowed, see readme-en.txt).
    "formatRightVehicle": "<font face='mono' width='24' color='{{c:rating}}'>{{rating%2.d~%|--%}}</font>|<font face='mono' width='30' color='{{c:wn8}}'>{{wn8%4.d|----}}</font>|<font face='mono' width='20' color='{{c:kb}}'>{{kb%2.d~k|--k}}</font>|<font color='{{c:vtype}}' alpha='{{alive?#FF|#60}}'>{{vehicle}}</font>"
  }
}
