﻿/** Heke's XVM Config - 11 June 2015
 * Options for alive without Alt markers.
 */
{
  // Floating damage values.
  "damageText": {
    // false - disable
    "visible": true,
    // Axis field coordinates
    "x": 0,
    "y": -67,
    // Opacity (dynamic transparency allowed, see macros.txt).
    "alpha": 100,
    // Color (dynamic colors allowed, see macros.txt).
    "color": null,
    "font": {
      "name": "$FieldFont",           // Font name   
      "size": 18,                     // Font size   
      "align": "center",              // Text alignment (left, center, right) / 
      "bold": false,                  // True - bold  
      "italic": false                 // True - italic 
    },
    // Параметры тени.
    "shadow": {
      "alpha": 100,                   // Opacity        
      "color": "0x000000",            //                 
      "angle": 45,                    // Offset angle    
      "distance": 0,                  // Offset distance  
      "size": 6,                      //                   
      "strength": 200                 // Intensity        
    },
    // Rising speed of displayed damage (float up speed).
    "speed": 2,
    // Maximum distance of target for which damage rises.
    "maxRange": 40,
    // Text for normal damage (see description of macros in the macros.txt).
    "damageMessage": "{{dmg}}",
    // Text for ammo rack explosion (see description of macros in the macros.txt).
    "blowupMessage": "{{l10n:blownUp}}\n{{dmg}}"
  },
  // Настройки для союзников.
  "ally": {
    // Type of vehicle icon (HT/MT/LT/TD/Arty).
    "vehicleIcon": {
      // false - disable
      "visible": true,
      // true - show speaker even if visible=false
      "showSpeaker": true,
      // Axis field coordinates
      "x": 0,
      "y": -20,
      // Opacity.
      "alpha": 100,
      // Color (currently not in use).
      "color": null,
      // Maximum scale (default is 100).
      "maxScale": 130,
      // Offset along the X axis (?)
      "scaleX": 0,
      // Offset along the Y axis (?)
      "scaleY": 16,
      "shadow": {
        "alpha": 100,                   // Opacity          
        "color": "0x000000",            //                  
        "angle": 45,                    // Offset angle     
        "distance": 0,                  // Offset distance  
        "size": 6,                      //                  
        "strength": 200                 // Intensity        
      }
    },
    "healthBar": {
      "visible": true,                  //   false -
      "x": -41,                         //   
      "y": -33,                         //  
      "alpha": 100,                     //   
      "color": null,                    //  
      "lcolor": null,                   //  
      "width": 80,                      //   
      "height": 12,                     //   
      "border": {
        "alpha": 30,                    //    
        "color": "0x000000",            //     
        "size": 1                       //     
      },
      "fill": {
        "alpha": 30                     //    
      },
      "damage": {
        "alpha": 100,                   //    
        "color": "{{c:dmg}}",           //     
        "fade": 1                       //     
      }
    },
    // Floating damage values for ally, player, squadman.
    "damageText": {
      "$ref": { "path":"damageText" }
    },
    "damageTextPlayer": {
      "$ref": { "path":"damageText" }
    },
    "damageTextSquadman": {
      "$ref": { "path":"damageText" }
    },
    // Vehicle contour icon.
    // Контурная иконка танка.
    "contourIcon": {
      // false - disable / не отображать.
      "visible": false,
      // Axis field coordinates.
      // Положение поля по осям.
      "x": 6,
      "y": -65,
      // Opacity (dynamic transparency allowed, see macros.txt).
      // Прозрачность (допускается использование динамической прозрачности, см. macros.txt).
      "alpha": 100,
      // Color (dynamic colors allowed, see macros.txt).
      // Цвет (допускается использование динамического цвета, см. macros.txt).
      "color": null,
      // Color intensity from 0 to 100. The default is 0 (off).
      // Интенсивность цвета от 0 до 100. По умолчанию 0, т.е. выключено.
      "amount": 0
    },
    // Player or clan icon.
    // Иконка игрока или клана.
    "clanIcon": {
      "visible": false,  // false - disable        / не отображать.
      "x": 0,            // Position on the X axis / Положение по оси X.
      "y": -67,          // Position on the Y axis / Положение по оси Y.
      "w": 16,           // Width                  / Ширина.
      "h": 16,           // Height                 / Высота.
      // Opacity (dynamic transparency allowed, see macros.txt).
      // Прозрачность (допускается использование динамической прозрачности, см. macros.txt)
      "alpha": 100
    },
    // Vehicle tier.
    "levelIcon": {
      "visible": false,  // false - disable        / не отображать.
      "x": 0,            // Position on the X axis / Положение по оси X.
      "y": -21,          // Position on the Y axis / Положение по оси Y.
      "alpha": 100       // Opacity                / Прозрачность.
    },
    // Markers "Help!" and "Attack!".
    // Маркеры "Нужна помощь" и "Атакую".
    "actionMarker": {
      "visible": true,   // false - disable        / не отображать.
      "x": 0,            // Position on the X axis / Положение по оси X.
      "y": -67,          // Position on the Y axis / Положение по оси Y.
      "alpha": 100       // Opacity                / Прозрачность.
    },
    // Block of text fields.
    // Блок текстовых полей.
    "textFields": [
      // Text field with the name of the tank.
      {
        "name": "Tank name",
        "visible": true,
        "x": 0,
        "y": -36,
        "alpha": 100,
        "color": null,
        "font": {
          "name": "$FieldFont",
          "size": 13,
          "align": "center",
          "bold": false,
          "italic": false
        },
        "shadow": {
          "alpha": 100,
          "color": "0x000000",
          "angle": 45,
          "distance": 0,
          "size": 6,
          "strength": 200
        },
        "format": "{{vehicle}}{{turret}}"	//"<font face='Wingdings' color='{{c:wn8}}' size='18'>M</font><font size='13'>{{vehicle}}{{turret}}</font>"
      },
      // Text field with the remaining / maximum health.
      {
        "name": "Tank HP",
        "visible": true,
        "x": 0,
        "y": -20,
        "alpha": 100,
        "color": "0xFCFCFC",
        "font": {
          "name": "$FieldFont",
          "size": 11,
          "align": "center",
          "bold": true,
          "italic": false
        },
        "shadow": {
          "alpha": 100,
          "color": "0x000000",
          "angle": 45,
          "distance": 0,
          "size": 4,
          "strength": 100
        },
        "format": "{{hp}} / {{hp-max}}"
      }
    ]
  },
  
  "enemy": {
    // Type of vehicle icon (HT/MT/LT/TD/Arty).
    "vehicleIcon": {
      "visible": true,
      "showSpeaker": true,
      "x": 0,
      "y": -20,
      "alpha": 100,
      "color": null,
      "maxScale": 130,
      "scaleX": 0,
      "scaleY": 16,
      "shadow": {
        "alpha": 100,
        "color": "0x000000",
        "angle": 45,
        "distance": 0,
        "size": 6,
        "strength": 200
      }
    },
    // Индикатор здоровья.
    "healthBar": {
      "visible": true,
      "x": -41,
      "y": -33,
      "alpha": 100,
      "color": null,
      "lcolor": null,
      "width": 80,
      "height": 12,
      "border": {
        "alpha": 30,
        "color": "0x000000",
        "size": 1
      },
      "fill": {
        "alpha": 30
      },
      "damage": {
        "alpha": 100,
        "color": "{{c:dmg}}",
        "fade": 1
      }
    },
    // Floating damage values for ally, player, squadman.
    // Всплывающий урон для союзника, игрока, взводного.
    "damageText": {
      "$ref": { "path":"damageText" }
    },
    "damageTextPlayer": {
      "$ref": { "path":"damageText" }
    },
    "damageTextSquadman": {
      "$ref": { "path":"damageText" }
    },
    // Vehicle contour icon.
    // Контурная иконка танка.
    "contourIcon": {
      "visible": false,
      "x": 6,
      "y": -65,
      "alpha": 100,
      "color": "{{c:wn8}}",
      "amount": 0
    },
    // Player or clan icon.
    // Иконка игрока или клана.
    "clanIcon": {
      "visible": false,
      "x": 0,
      "y": -67,
      "w": 16,
      "h": 16,
      "alpha": 100
    },
    // Vehicle tier.
    // Уровень танка.
    "levelIcon": {
      "visible": false,
      "x": 0,
      "y": -21,
      "alpha": 100
    },
    // Markers "Help!" and "Attack!".
    "actionMarker": {
      "visible": true,
      "x": 0,
      "y": -67,
      "alpha": 100
    },
    // Block of text fields.
    "textFields": [
      // Text field with the name of the tank.
      {
        "name": "Tank name",
        "visible": true,
        "x": 0,
        "y": -36,
        "alpha": 100,
        "color": null,
        "font": {
          "name": "$FieldFont",
          "size": 13,
          "align": "center",
          "bold": false,
          "italic": false
        },
        "shadow": {
          "alpha": 100,
          "color": "0x000000",
          "angle": 45,
          "distance": 0,
          "size": 6,
          "strength": 200
        },
        "format": "{{vehicle}}{{turret}}"	//"<font face='Wingdings' color='{{c:wn8}}' size='18'>M</font><font size='13'>{{vehicle}}{{turret}}</font>"
      },
      // Text field with the remaining / maximum health.
      {
        "name": "Tank HP",
        "visible": true,
        "x": 0,
        "y": -20,
        "alpha": 100,
        "color": "0xFCFCFC",
        "font": {
          "name": "$FieldFont",
          "size": 11,
          "align": "center",
          "bold": true,
          "italic": false
        },
        "shadow": {
          "alpha": 100,
          "color": "0x000000",
          "angle": 45,
          "distance": 0,
          "size": 4,
          "strength": 100
        },
        "format": "{{hp}} / {{hp-max}}"
      }
    ]
  }
}