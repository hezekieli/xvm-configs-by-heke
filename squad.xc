﻿/** Heke's XVM Config - 11 June 2015
 * Parameters for squad window.
 */
{
  "squad": {
    // false - Disable display info.
    "enabled": true,
    // false - hide player clan.
    "showClan": true,
    // Format of vehicle info field.
    "formatInfoField": "{{rlevel}}"
  }
}
