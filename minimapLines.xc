﻿/** Heke's XVM Config - 11 June 2015
 * Minimap lines. Only for owned vehicle.
 */
{
  "def": {
     // "inmeters": true  - make line size to be in real map meters.
     // "inmeters": false - make line size to be in minimap interface clip points. Minimap interface clip side is 210 points.
	 
     // Own vehicle direction definition.
    "vehicle": 			{ "enabled": true, "inmeters": true, "color": "0xFFFFFF" },	// 0xFFCC66
     // Camera direction definition.
    "camera": 			{ "enabled": true, "inmeters": true, "color": "0xFFCC66" },	// 0x60FF00
     // Dots definition.
    "dot": 				{ "enabled": true, "inmeters": true, "color": "0xFFCC66" },	// 0x0099FF
     // Horizontal gun traverse angle definition.
    "traverseAngle": 	{ "enabled": true, "inmeters": true, "color": "0xAAAAAA" }
  },
  "lines": {
       "enabled": true,
       // Distance between farthest corners at 1km map is somewhat more than 1400 meters.
       // Sections can contain any number of lines.
       // To set a point try setting line with length of one and large thickness.
       // You can leave one line for simplicity. Remember comma positioning rules.
       //---------------------------------------------------------------------------------------------------
       // Own vehicle direction.
       "vehicle": [
         { "$ref": { "path": "def.vehicle" }, "from": 50,  "to": 97, "thickness": 1.5, "alpha": 50 },
         { "$ref": { "path": "def.vehicle" }, "from": 100,  "to": 147, "thickness": 1.4, "alpha": 48 },
         { "$ref": { "path": "def.vehicle" }, "from": 150,  "to": 197, "thickness": 1.3, "alpha": 46 },
         { "$ref": { "path": "def.vehicle" }, "from": 200, "to": 248, "thickness": 1.2, "alpha": 44 },
         { "$ref": { "path": "def.vehicle" }, "from": 250, "to": 298, "thickness": 1.1, "alpha": 43 },
         { "$ref": { "path": "def.vehicle" }, "from": 300, "to": 398, "thickness": 1, "alpha": 40 },
         { "$ref": { "path": "def.vehicle" }, "from": 400, "to": 498, "thickness": 0.9, "alpha": 40 },
         { "$ref": { "path": "def.vehicle" }, "from": 500, "to": 2000, "thickness": 0.75, "alpha": 40 }
       ],
       // Camera direction.
       "camera": [
         { "$ref": { "path": "def.camera" }, "from": 50, "to": 75, "thickness": 1.3, "alpha": 50 },
         { "$ref": { "path": "def.camera" }, "from": 125, "to": 175, "thickness": 1.2, "alpha": 45 },
         { "$ref": { "path": "def.camera" }, "from": 225, "to": 275, "thickness": 1.1, "alpha": 40 },
         { "$ref": { "path": "def.camera" }, "from": 325, "to": 375, "thickness": 1, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 425, "to": 475, "thickness": 0.9, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 525, "to": 575, "thickness": 0.8, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 625, "to": 675, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 720, "to": 780, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 820, "to": 880, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 920, "to": 980, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 1020, "to": 1080, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 1120, "to": 1180, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 1220, "to": 1280, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 1320, "to": 1380, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 1420, "to": 1480, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 1520, "to": 1580, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 1620, "to": 1680, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 1720, "to": 1780, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 1820, "to": 1880, "thickness": 0.75, "alpha": 35 },
         { "$ref": { "path": "def.camera" }, "from": 1920, "to": 2000, "thickness": 0.75, "alpha": 35 },
           //Dots
         { "$ref": { "path": "def.dot" }, "from": 99.5, "to": 100.5, "thickness": 2.5, "alpha": 80 },
         { "$ref": { "path": "def.dot" }, "from": 199.4, "to": 200.4, "thickness": 2.4, "alpha": 75 },
         { "$ref": { "path": "def.dot" }, "from": 299.3, "to": 300.3, "thickness": 2.3, "alpha": 70 },
         { "$ref": { "path": "def.dot" }, "from": 399.2, "to": 400.2, "thickness": 2.2, "alpha": 65 },
         { "$ref": { "path": "def.dot" }, "from": 499.1, "to": 500.1, "thickness": 2.1, "alpha": 60 },
         { "$ref": { "path": "def.dot" }, "from": 599, "to": 600, "thickness": 2.0, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 699, "to": 700, "thickness": 1.9, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 799, "to": 800, "thickness": 1.8, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 899, "to": 900, "thickness": 1.7, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 999, "to": 1000, "thickness": 1.6, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 1099, "to": 1100, "thickness": 1.5, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 1199, "to": 1200, "thickness": 1.5, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 1299, "to": 1300, "thickness": 1.5, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 1399, "to": 1400, "thickness": 1.5, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 1499, "to": 1500, "thickness": 1.5, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 1599, "to": 1600, "thickness": 1.5, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 1699, "to": 1700, "thickness": 1.5, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 1799, "to": 1800, "thickness": 1.5, "alpha": 55 },
         { "$ref": { "path": "def.dot" }, "from": 1899, "to": 1900, "thickness": 1.5, "alpha": 55 }
       ],
       // Gun traverse angles may differ depending on vehicle angle relative to ground. See pics at http://goo.gl/ZqlPa
       //---------------------------------------------------------------------------------------------------
       // Horizontal gun traverse angle lines.
       "traverseAngle": [
         { "$ref": { "path": "def.traverseAngle" }, "from": 50, "to": 97, "thickness": 0.6, "alpha": 50 },
         { "$ref": { "path": "def.traverseAngle" }, "from": 100, "to": 147, "thickness": 0.6, "alpha": 48 },
         { "$ref": { "path": "def.traverseAngle" }, "from": 150, "to": 197, "thickness": 0.6, "alpha": 46 },
         { "$ref": { "path": "def.traverseAngle" }, "from": 200, "to": 248, "thickness": 0.6, "alpha": 44 },
         { "$ref": { "path": "def.traverseAngle" }, "from": 250, "to": 298, "thickness": 0.6, "alpha": 42 },
         { "$ref": { "path": "def.traverseAngle" }, "from": 300, "to": 398, "thickness": 0.6, "alpha": 40 },
         { "$ref": { "path": "def.traverseAngle" }, "from": 400, "to": 498, "thickness": 0.6, "alpha": 40 },
         { "$ref": { "path": "def.traverseAngle" }, "from": 500, "to": 2000, "thickness": 0.6, "alpha": 40 }
       ]
    }
}