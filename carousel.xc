﻿/**
 * Parameters for tank carousel
 */
{
  "carousel": {
    // false - Disable customizable carousel.
    "enabled": true,
    // Scale of carousel cells.
    "zoom": 1,
    // Number of rows at carousel.
    "rows": 1,
    // Spacing between carousel cells.
    "padding": {
        "horizontal": 10,   // по горизонтали
        "vertical": 2       // по вертикали
    },
    // true - show filters even if all tanks fit on the screen.
    "alwaysShowFilters": false,
    // true - hide cell "Buy tank".
    "hideBuyTank": false,
    // true - hide cell "Buy slot".
    "hideBuySlot": false,
    // Visibility filters.
    "filters": {
      // false - hide filter.
      "nation":   { "enabled": true },  // nation          
      "type":     { "enabled": true },  // vehicle class   
      "level":    { "enabled": true },  // vehicle level   
      "favorite": { "enabled": true },  // favorite tanks  
      "prefs":    { "enabled": true }   // other filters   
    },
    // Standard cell elements.
    "fields": {
      // "visible"  - the visibility of the element 
      // "dx"       - horizontal shift              
      // "dy"       - vertical shift                
      // "alpha"    - transparency                  
      // "scale"    - scale                         
      //
      // Vehicle class icon.
      "tankType": { "visible": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Vehicle level.
      "level":    { "visible": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // todo: english description
      "multiXp":  { "visible": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // todo: english description
      "xp":       { "visible": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Vehicle name.
      "tankName": { "visible": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Status text (Crew incomplete, Repairs required)
      "statusText": { "visible": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Clan lock timer
      "clanLock":   { "visible": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 }
    },
    // Extra cell fields (see playersPanel.xc).
    "extraFields": [
      // Sign of mastery.
      { "x": -1, "y": 10, 	"format": "<img src='img://gui/maps/icons/library/proficiency/class_icons_{{v.mastery}}.png' width='23' height='23'>" },
	  //{ "x": 1, "y": 50, 	"format": "<font face='mono' size='13' color='{{v.c_wn8effd|#666666}}'>{{v.wn8effd%-4.02f|-.--}}</font>" },		// Tank specific Wn8 Effective Damage / Average Damage
	  //{ "x": 1, "y": 50, 	"format": "<font face='mono' size='13' color='{{v.c_wn8|#666666}}'>{{v.wn8|--}}</font>" },		// Tank specific Wn8 Effective Damage / Average Damage
	  { "x": 1, "y": 50, 	"format": "<font face='mono' size='13' color='{{v.c_xte|#666666}}'>{{v.xte|--}}</font>" },						// Tank specific XVM-rating 0-100
	  { "x": 1, "y": 64, 	"format": "<font face='mono' size='13' color='{{v.c_winrate|#666666}}'>{{v.winrate%d~%|--%}}</font>" },			// Tank specific Winrate
	  { "x": 40, "y": -0.7,	"format": "<font face='mono' size='12' color='#AAAA87'>({{v.battletiermin}}-{{v.battletiermax}})</font>" },		// Tank Battle Tier range
	  { "x": 109, "y": 50, 	"format": "<font face='mono' size='12' color='#CC7070'>{{v.xpToEliteLeft%'6.6s}}</font><font face='xvm' size='13' color='#CC7070' alpha='{{v.xpToEliteLeft?#FF|#00}}'>&#x21;</font>" },	// &#x21; for
	  { "x": 109, "y": 64, 	"format": "<font face='mono' size='12' color='#DDDDFF'>{{v.earnedXP%'6.6s}}</font><font face='xvm' size='13' color='#DDDDFF' alpha='{{v.earnedXP?#FF|#00}}'>&#x21;</font>" }			// &#x21; for star
	  
    ],
    // Order of nations.
    //"nations_order": ["ussr", "germany", "usa", "france", "uk", "china", "japan"],
    "nations_order": [],
    // Order of types of vehicles.
    "types_order":   ["lightTank", "mediumTank", "heavyTank", "AT-SPG", "SPG"],
    // Tank sorting criteria, available options: (minus = reverse order)
    // "nation", "type", "level", "maxBattleTier", "premium", "-level", "-maxBattleTier", "-premium"
    "sorting_criteria": ["nation", "type", "level"],
    // Suppress the tooltips for tanks in carousel
    "suppressCarouselTooltips": false
  }
}
