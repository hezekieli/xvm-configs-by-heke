﻿/** Heke's XVM Config - 11 June 2015
 * Minimap circles. Only real map meters. Only for own unit.
 */
{
    "circles": {
        "enabled": true,
        // TODO: better description and translation
        // View distance
        // :
        //   "enabled": false - 
        //   "distance" - 
        //   "scale" -   ( ) (  )
        //   "thickness" - 
        //   "alpha" - 
        //   "color" - 
        //   "state" -  : 1-, 2- (  )
        //   :
        //   N -   ,   
        //   "dynamic"   -     c  /
        //   "motion"    -      
        //   "standing"  -     
        //   "blindarea" -      (50<=X<=445)
        //   "blindarea_motion" -        (50<=X<=445)
        //   "blindarea_standing" -       (50<=X<=445)
        // :
        //   http://www.koreanrandom.com/forum/topic/15467-/page-5#entry187139
        //   http://www.koreanrandom.com/forum/topic/15467-/page-4#entry186794
        "view": [
            // Main circles:
            { "enabled":  true, "distance": "blindarea", "scale": 1, "thickness": 0.5, "alpha": 80, "color": "0x3EB5F1" },
            { "enabled":  true, "distance": 445,         "scale": 1, "thickness":  0.5, "alpha": 60, "color": "0xFFF066" },
            { "enabled":  false, "distance": 700,         "scale": 1, "thickness":  0.5, "alpha": 60, "color": "0x000000" },
			// Circle of the maximum units appearance.
            { "enabled": true, "distance": 564,         "scale": 1, "thickness":  0.5, "alpha": 50, "color": "0x99FF99" },
            // Additional circles:
            { "enabled": true, "distance": 50,          "scale": 1, "thickness":  0.5, "alpha": 50, "color": "0xFFFFFF" },
			{ "enabled": true, "distance": 100,          "scale": 1, "thickness":  0.4, "alpha": 20, "color": "0xFFFFFF" },
			{ "enabled": true, "distance": 200,          "scale": 1, "thickness":  0.4, "alpha": 20, "color": "0xFFFFFF" },
			{ "enabled": true, "distance": 300,          "scale": 1, "thickness":  0.4, "alpha": 20, "color": "0xFFFFFF" },
            { "enabled": false, "distance": "standing",  "scale": 1, "thickness":  1.0, "alpha": 60, "color": "0x00FF99" },
            { "enabled": false, "distance": "motion",    "scale": 1, "thickness":  1.0, "alpha": 60, "color": "0x00FF99" },
            { "enabled": true, "distance": "dynamic",   "scale": 1, "thickness":  0.5, "alpha": 60, "color": "0x0099FF" }
        ],
        // Maximum range of fire for artillery
        // Artillery gun fire range may differ depending on vehicle angle relative to ground
        // and vehicle height positioning relative to target. These factors are not considered.
        // See pics at http://goo.gl/ZqlPa
        // ------------------------------------------------------------------------------------------------
        //     
        //             
        //      .      .
        //   : http://goo.gl/ZqlPa
        "artillery": { "enabled": true, "alpha": 50, "color": "0x000000", "thickness": 0.5 },
        // Maximum range of shooting for machine gun
        //       
        "shell":     { "enabled": true, "alpha": 50, "color": "0x000000", "thickness": 0.5 },
        // Special circles dependent on vehicle type.
        // Many configuration lines for the same vehicle make many circles.
        // See other vehicle types at (replace : symbol with -):
        // http://code.google.com/p/wot-xvm/source/browse/trunk/src/xpm/xvmstat/vehinfo_short.py
        // ------------------------------------------------------------------------------------------------
        //  ,    .
        //        .
        //        ( :  -):
        // http://code.google.com/p/wot-xvm/source/browse/trunk/src/xpm/xvmstat/vehinfo_short.py
        "special": [
          // Example: Artillery gun fire range circle
          // :    
          // "enabled": false - ; "thickness" - ; "alpha" - ; "color" - .
          //{ "ussr-SU-18": { "enabled": true, "thickness": 1, "alpha": 60, "color": "0xEE4444", "distance": 500 } },
        ]
    }
}