﻿/**
 * Text substitutions.
 * Heke's XVM Config 11 June 2015
 */
{
  "texts": {
    // Text for {{vtype}} macro.
    "vtype": {
		// Text for Light Tanks / Text for light tanks. 
		"LT": "& # x69;", 
		// Text for Medium Tanks / Text for medium tanks. 
		"MT": "& # x68;" , 
		// Text for Heavy Tanks / Text for heavy tanks. 
		"HT": "& # x67;", 
		// Text for Arty / text for the arts. 
		"SPG": "& # X6B;", 
		// Text for Tank destroyers / text for the TP. 
		"TD": "& # X6A;"
		/*
		// Text for Light Tanks / Text for light tanks. 
		"LT": "i", 
		// Text for Medium Tanks / Text for medium tanks. 
		"MT": "h" , 
		// Text for Heavy Tanks / Text for heavy tanks. 
		"HT": "g", 
		// Text for Arty / text for the arts. 
		"SPG": "k", 
		// Text for Tank destroyers / text for the TP. 
		"TD": "j"
		
		// Text for light tanks
		"LT":  "LT",	//"{{l10n:LT}}",
		// Text for medium tanks
		"MT":  "MT",	//"{{l10n:MT}}",
		// Text for heavy tanks
		"HT":  "HT",	//"{{l10n:HT}}",
		// Text for arty
		"SPG": "SPG",	//"{{l10n:SPG}}",
		// Text for tank destroyers
		"TD":  "TD"	//"{{l10n:TD}}"
		*/
    },
    // Text for {{marksOnGun}}, {{v.marksOnGun}} macros.
    // ????? ??? ???????? {{marksOnGun}}, {{v.marksOnGun}}
    "marksOnGun": {
      "_0": "0",
      "_1": "1",
      "_2": "2",
      "_3": "3"
    },
    // Text for {{spotted}} macro.
    // ????? ??? ??????? {{spotted}}
    "spotted": {
      "neverSeen": "",
      "lost": "<font face='Webdings' size='18' color='#C0C0C0'>N</font>",
      "revealed": "<font face='Webdings' size='18' color='#00A9FF'>N</font>",
      "dead": "<font face='Wingdings' size='20' color='#331010'>N</font>",
      "neverSeen_arty": "",
      "lost_arty": "<font face='Webdings' size='18' color='#C0C0C0'>N</font>",
      "revealed_arty": "<font face='Webdings' size='18' color='#00A9FF'>N</font>",
      "dead_arty": "<font face='xvm' size='20' color='#331010'>&#x2B;</font>"
    },
    // Text for {{xvm-user}} macro.
    // ????? ??? ??????? {{xvm-user}}
    "xvmuser": {
      // XVM with enabled statistics / XVM ?? ?????????? ???????????.
      "on": "on",
      // XVM with disabled statistics / XVM ?? ??????????? ???????????.
      "off": "off",
      // Without XVM / ??? XVM.
      "none": "none"
    },
    // Text for {{battletype}} macro.
    // ????? ??? ??????? {{battletype}}
    "battletype": {
      "unknown": "unknown",
      "regular": "regular",
      "training": "training",
      "company": "company",
      "tournament": "tournament",
      "clan": "clan",
      "tutorial": "tutorial",
      "cybersport": "cybersport",
      "historical": "historical",
      "event_battles": "event battles",
      "sortie": "sortie",
      "fort_battle": "stronghold battle",
      "rated_cybersport": "rated cybersport"
    },
    // Text for {{topclan}} macro.
    // ????? ??? ??????? {{topclan}}
    "topclan": {
      "top": "top",
      "persist": "persist",
      "regular": ""
    }
  }
}
