﻿/** Heke's XVM Config - 11 June 2015
 * Extra sounds settings.
 */
{
  "sounds": {
    // Sound id for sixth sense sound (from gui_sounds.xml). Format: /file/group/sound
    // Id звука для шестого чувства (из gui_sounds.xml). Формат: /файл/группа/звук
    "sixthSense": "/xvm/xvm/sixthsense"
  }
}
