﻿/** Heke's XVM Config - 11 June 2015
 * Minimap labels. Basic HTML/CSS supported.
 */
{
	"labels": {
		// Maximum nickname size for {{short-nick}} macro. Ex. Squad members
		"nickShrink": 6,
		// {{vehicle-class}} macro substitutions.
		"vehicleclassmacro": {
			"light": 	"<font face='vtype' size='10'>&#x69;</font>",	// &#x3A;
			"medium": 	"<font face='vtype' size='10'>&#x68;</font>",	// &#x3B;
			"heavy": 	"<font face='vtype' size='10'>&#x67;</font>",	// &#x3F;
			"td": 		"<font face='vtype' size='10'>&#x6A;</font>",	// &#x2E;
			"spg": 		"<font face='vtype' size='10'>&#x6B;</font>",	// &#x2D;
			"superh": 	"<font face='vtype' size='10'>&#x6C;</font>"
		},
        // Special symbols website
        // http://www.fileformat.info/info/unicode/char/25a0/index.htm
        // Great symbolic font by Andrey_Hard for {{vehicle-class}}:
        // http://goo.gl/d2KIj
		
		// Textfields for tanks on minimap.
		"units": {
			// Textfields switch for revealed units.
			"revealedEnabled": true,
			// Textfields switch for lost enemy units. Show last seen position.
			"lostEnemyEnabled": true,
			// Textfields antialias type.
			"antiAliasType": "advanced", // normal/advanced
			
			"format": {
				"ally":				"<font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_a'><font color='{{c:wn8}}'>{{name%.8s~..}}</font></span>",
				"teamkiller":		"<font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_t'><font color='{{c:wn8}}'>{{name%.8s~..}}</font></span>",
				"enemy":			"<font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_e'><font color='{{c:wn8}}'>{{name%.8s~..}}</font></span>",
				"squad":			"<font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_s'><font color='{{c:wn8}}'>{{name%.8s~..}}</font></span>",
				// Own marker or spectated subject.
				"oneself":        	"<font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio=100?   |{{hp-ratio%.436a}}}}</font>",	// <font face='dynamic' size='16' color='#333333' alpha='#A0'>&#536;</font>
				// Out of radio range ally    
				"lostally":			"<textformat leading='-13'><span class='mm_dot'> {{vehicle-class}}</span><br><font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_la'><font color='{{c:wn8}}'>{{name%.8s~..}}</font></span></textformat>",
				// Out of radio range teamkiller
				"lostteamkiller":	"<textformat leading='-13'><span class='mm_dot'> {{vehicle-class}}</span><br><font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_lt'><font color='{{c:wn8}}'>{{name%.8s~..}}</font></span></textformat>",
				// Lost enemy units.
				"lost":           	"<textformat leading='-13'><span class='mm_dot'> {{vehicle-class}}</span><br><font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_l'><font color='{{c:wn8}}'>{{name%.8s~..}}</font></span></textformat>",	// thin space &#8201; non-breaking space &#8239;
				// Out of radio range squadman
				"lostsquad":		"<textformat leading='-13'><span class='mm_dot'> {{vehicle-class}}</span><br><font face='dynamic' size='16' color='{{c:hp-ratio}}'>{{hp-ratio%.436a|   }}</font><span class='mm_ls'><font color='{{c:wn8}}'>{{name%.8s~..}}</font></span></textformat>",
				"deadally":			"<span class='mm_dot'>{{vehicle-class}}</span><span class='mm_da'> {{vehicle}}</span>",
				"deadteamkiller":	"<span class='mm_dot'>{{vehicle-class}}</span><span class='mm_dt'> {{vehicle}}</span>",
				"deadenemy":		"<span class='mm_dot'>{{vehicle-class}}</span><span class='mm_de'> {{vehicle}}</span>",
				"deadsquad":		"<span class='mm_dot'>{{vehicle-class}}</span><span class='mm_ds'> {{vehicle}}</span>"
			},
			// CSS style (fonts and colors option)
			"css": {
				"ally":            ".mm_a{font-family:$FieldFont; font-size:6px; color:#CFCFCF;}",//color:#C8FFA6;}",
				"teamkiller":      ".mm_t{font-family:$FieldFont; font-size:6px; color:#CFCFCF;}",//color:#A6F8FF;}",
				"enemy":           ".mm_e{font-family:$FieldFont; font-size:6px; color:#CFCFCF;}",//color:#FCA9A4;}",
				"squad":           ".mm_s{font-family:$FieldFont; font-size:6px; color:#CFCFCF;}",//color:#FFD099;}",
				"oneself":         ".mm_o{font-family:$FieldFont; font-size:8px; color:#CFCFCF;}",//color:#FFFFFF;}",
				"lostally":       ".mm_la{font-family:$FieldFont; font-size:6px; color:#CFCFCF;} .mm_dot{font-family:xvmsymbol; font-size:12px; color:#B4E595;}",
				"lostteamkiller": ".mm_lt{font-family:$FieldFont; font-size:6px; color:#CFCFCF;} .mm_dot{font-family:xvmsymbol; font-size:12px; color:#00D2E5;}",
				"lost":            ".mm_l{font-family:$FieldFont; font-size:6px; color:#CFCFCF;} .mm_dot{font-family:xvmsymbol; font-size:12px; color:#E97069;}",
				"lostsquad":      ".mm_ls{font-family:$FieldFont; font-size:6px; color:#CFCFCF;} .mm_dot{font-family:xvmsymbol; font-size:12px; color:#E5BB8A;}",
				"deadally":       ".mm_da{font-family:$FieldFont; font-size:6px; color:#A0A0A0;} .mm_dot{font-family:vtype; font-size:10px; color:#A0A0A0;}",
				"deadteamkiller": ".mm_dt{font-family:$FieldFont; font-size:6px; color:#A0A0A0;} .mm_dot{font-family:vtype; font-size:10px; color:#A0A0A0;}",
				"deadenemy":      ".mm_de{font-family:$FieldFont; font-size:6px; color:#A0A0A0;} .mm_dot{font-family:vtype; font-size:10px; color:#A0A0A0;}",
				"deadsquad":      ".mm_ds{font-family:$FieldFont; font-size:6px; color:#A0A0A0;} .mm_dot{font-family:vtype; font-size:10px; color:#A0A0A0;}"
				/*
				"deadally":       ".mm_da{font-family:$FieldFont; font-size:6px; color:#CFFFCF;} .mm_dot{font-family:vtype; font-size:10px; color:#032900;}",
				"deadteamkiller": ".mm_dt{font-family:$FieldFont; font-size:6px; color:#CFFFFF;} .mm_dot{font-family:vtype; font-size:10px; color:#043A40;}",
				"deadenemy":      ".mm_de{font-family:$FieldFont; font-size:6px; color:#FFCFCF;} .mm_dot{font-family:vtype; font-size:10px; color:#4D0300;}",
				"deadsquad":      ".mm_ds{font-family:$FieldFont; font-size:6px; color:#FFEFCF;} .mm_dot{font-family:vtype; font-size:10px; color:#663800;}"
				*/
			},
			// Fields shadow.
			// "distance"- , "angle"- > 0, "alpha"- , "blur"- , "strength'-
			"shadow": {
				"ally":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"teamkiller":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"enemy":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"squad":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"oneself":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"lostally":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"lostteamkiller":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"lost":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"lostsquad":
				{ "enabled": true, "color": "0x000000", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 2 },
				"deadally":
				{ "enabled": true, "color": "0x032900", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 3 },
				"deadteamkiller":
				{ "enabled": true, "color": "0x043A40", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 3 },
				"deadenemy":
				{ "enabled": true, "color": "0x4D0300", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 3 },
				"deadsquad":
				{ "enabled": true, "color": "0x663800", "distance": 0, "angle": 45, "alpha": 80, "blur": 2, "strength": 3 }
			},
			// Field offset relative to current icon (except lost - relative to enemy last seen position).
			"offset": {
				"ally":           {"x": -7.5, "y": -9.6},
				"teamkiller":     {"x": -7.5, "y": -9.6},
				"enemy":          {"x": -7.5, "y": -9.6},
				"squad":          {"x": -7.5, "y": -9.6},
				"oneself":        {"x": -7.5, "y": -9.6},
				"lostally":       {"x": -7.6, "y": -9.4},
				"lostteamkiller": {"x": -7.6, "y": -9.4},
				"lost":           {"x": -7.6, "y": -9.4},
				"lostsquad":      {"x": -7.6, "y": -9.4},
				"deadally":       {"x": -4.6, "y": -7.7},
				"deadteamkiller": {"x": -4.6, "y": -7.7},
				"deadenemy":      {"x": -4.6, "y": -7.7},
				"deadsquad":      {"x": -4.6, "y": -7.7}
			},
			"alpha" : {
				"ally": 100,
				"teamkiller": 100,
				"enemy": 100,
				"squad": 100,
				"oneself": 100,
				"lostally": 90,
				"lostteamkiller": 90,
				"lost": 90,
				"lostsquad": 90,
				"deadally": 70,
				"deadteamkiller": 70,
				"deadenemy": 70,
				"deadsquad": 70
			}
		},
		// Textfield for map side size. 1000m, 700m, 600m.
		"mapSize": {
			"enabled": true,
			"format": "<b>{{cellsize}}0 m</b>",
			"css": "font-size:6px; font-family:$FieldFont; color:#959569;",	//"#89896C;",
			"alpha": 100,
			"offsetX": 1,
			"offsetY": 0,
			"shadow": {
				"enabled": true,
				"color": "0x000000",
				"distance": 0,
				"angle": 0,
				"alpha": 80,
				"blur": 3,
				"strength": 3
			},
			// Decrease sizes in case of map image weird shrinking while map resize.
			// Increase sizes in case of field being partially cut off.
			// -------------------------------------------------------------------------------------
			"width": 100,
			"height": 30
		}
	}
}