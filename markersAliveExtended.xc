﻿/** Heke's XVM Config - 11 June 2015
 * Options for alive with Alt markers.
 */
{
  // Floating damage values.
  "damageText": {
    // false - disable
    "visible": true,
    // Axis field coordinates
    "x": 0,
    "y": -67,
    // Opacity (dynamic transparency allowed, see macros.txt).
    "alpha": 100,
    // Color (dynamic colors allowed, see macros.txt).
    "color": null,
    "font": {
      "name": "$FieldFont",           // Font name      
      "size": 18,                     // Font size      
      "align": "center",              // Text alignment (left, center, right)
      "bold": false,                  // True - bold    
      "italic": false                 // True - italic 
    },
    // Параметры тени.
    "shadow": {
      "alpha": 100,                   // Opacity          
      "color": "0x000000",            //                    
      "angle": 45,                    // Offset angle     
      "distance": 0,                  // Offset distance 
      "size": 6,                      //                   
      "strength": 200                 // Intensity        
    },
    // Rising speed of displayed damage (float up speed).

    "speed": 2,
    // Maximum distance of target for which damage rises.
    "maxRange": 40,
    // Text for normal damage (see description of macros in the macros.txt).
    "damageMessage": "{{dmg}}",
    // Text for ammo rack explosion (see description of macros in the macros.txt).
    "blowupMessage": "{{l10n:blownUp}}\n{{dmg}}"
  },
  
  "ally": {
    // Type of vehicle icon (HT/MT/LT/TD/Arty).
    "vehicleIcon": {
      // false - disable / не отображать
      "visible": true,
      // true - show speaker even if visible=false
      "showSpeaker": false,
      // Axis field coordinates
      "x": 0,
      "y": -20,
      // Opacity.
      "alpha": 100,
      // Color (currently not in use).
      "color": null,
      // Maximum scale (default is 100).
      "maxScale": 130,
      // Offset along the X axis (?)
      "scaleX": 0,
      // Offset along the Y axis (?)
      "scaleY": 16,
      "shadow": {
        "alpha": 100,                   // Opacity          / Прозрачность.
        "color": "0x000000",            //                    Цвет.
        "angle": 45,                    // Offset angle     / Угол смещения.
        "distance": 0,                  // Offset distance  / Дистанция смещения.
        "size": 6,                      //                    Размер.
        "strength": 200                 // Intensity        / Интенсивность.
      }
    },

    "healthBar": {
      "visible": true,                  //   false - не отображать
      "x": -41,                         //   положение по оси X
      "y": -33,                         //   положение по оси Y
      "alpha": 100,                     //   прозрачность (допускается использование динамической прозрачности, см. macros.txt)
      "color": null,                    //   цвет основной (допускается использование динамического цвета, см. macros.txt)
      "lcolor": null,                   //   цвет дополнительный (для градиента)
      "width": 80,                      //   ширина полосы здоровья
      "height": 12,                     //   высота полосы здоровья
      "border": {
        "alpha": 30,                    //     прозрачность
        "color": "0x000000",            //     цвет
        "size": 1                       //     размер рамки
      },
      "fill": {
        "alpha": 30                     //     прозрачность
      },
      "damage": {
        "alpha": 80,                    //     прозрачность
        "color": null,                  //     цвет
        "fade": 1                       //     время затухания в секундах
      }
    },
    // Floating damage values for ally, player, squadman.
    "damageText": {
      "$ref": { "path":"damageText" }
    },
    "damageTextPlayer": {
      "$ref": { "path":"damageText" }
    },
    "damageTextSquadman": {
      "$ref": { "path":"damageText" }
    },
    // Vehicle contour icon.
    "contourIcon": {
      // false - disable / не отображать.
      "visible": true,
      // Axis field coordinates.
      "x": 6,
      "y": -74,
      // Opacity (dynamic transparency allowed, see macros.txt).
      "alpha": 100,
      // Color (dynamic colors allowed, see macros.txt).
      "color": "{{c:vtype}}",
      // Color intensity from 0 to 100. The default is 0 (off).
      "amount": 0
    },
    // Player or clan icon.
    "clanIcon": {
      "visible": true,  // false - disable        / не отображать.
      "x": -20,            // Position on the X axis / Положение по оси X.
      "y": -71,          // Position on the Y axis / Положение по оси Y.
      "w": 16,           // Width                  / Ширина.
      "h": 16,           // Height                 / Высота.
      // Opacity (dynamic transparency allowed, see macros.txt).
      // Прозрачность (допускается использование динамической прозрачности, см. macros.txt)
      "alpha": 100
    },
    // Vehicle tier.
    "levelIcon": {
      "visible": true,	// false - disable        / не отображать.
      "x": 0,            // Position on the X axis / Положение по оси X.
      "y": -80,          // Position on the Y axis / Положение по оси Y.
      "alpha": 100       // Opacity                / Прозрачность.
    },
    // Markers "Help!" and "Attack!".
    "actionMarker": {
      "visible": true,   // false - disable        / не отображать.
      "x": 0,            // Position on the X axis / Положение по оси X.
      "y": -67,          // Position on the Y axis / Положение по оси Y.
      "alpha": 100       // Opacity                / Прозрачность.
    },
    // Block of text fields.
    // Блок текстовых полей.
    "textFields": [
      // Text field with the name of the player.
      {
        "name": "Tank name",          // название текстового поля, ни на что не влияет
        "visible": true,                // false - не отображать
        "x": 0,                         // положение по оси X
        "y": -36,                       // положение по оси Y
        "alpha": 100,                   // прозрачность (допускается использование динамической прозрачности, см. macros.txt)
        "color": null,                  // цвет (допускается использование динамического цвета, см. macros.txt)
        "font": {
          "name": "$FieldFont",         //   название
          "size": 13,                   //   размер
          "align": "center",            //   выравнивание текста (left, center, right)
          "bold": false,                //   обычный (false) или жирный (true)
          "italic": false               //   обычный (false) или курсив (true)
        },
        // Параметры тени.
        "shadow": {
          "alpha": 100,                 //   прозрачность
          "color": "0x000000",          //   цвет
          "angle": 45,                  //   угол смещения
          "distance": 0,                //   дистанция смещение
          "size": 6,                    //   размер
          "strength": 200               //   интенсивность
        },
        "format": "{{vehicle}}{{turret}}"
      },
      // Text field with the percentage of remaining health.
      // Текстовое поле с процентом оставшегося здоровья.
      {
        "name": "Percent of HP",
        "visible": true,
        "x": 0,
        "y": -20,
        "alpha": 100,
        "color": "0xFCFCFC",
        "font": {
          "name": "$FieldFont",
          "size": 11,
          "align": "center",
          "bold": true,
          "italic": false
        },
        "shadow": {
          "alpha": 100,
          "color": "0x000000",
          "angle": 45,
          "distance": 0,
          "size": 4,
          "strength": 100
        },
        "format": "{{hp-ratio}}%"
      },
      // Text field with win ratio.
      {
        "name": "Player name",
        "visible": true,
        "x": 0,
        "y": -50,
        "alpha": 100,
        "color": "{{c:wn8}}",
        "font": {
          "name": "$FieldFont",
          "size": 12,
          "align": "center",
          "bold": true,
          "italic": false
        },
        "shadow": {
          "alpha": 100,
          "color": "0x000000",
          "angle": 45,
          "distance": 0,
          "size": 6,
          "strength": 200
        },
        "format": "{{nick}}"
      }
    ]
  },
  // Настройки для противников.
  "enemy": {
    // Type of vehicle icon (HT/MT/LT/TD/Arty).
    // Иконка типа танка (ТТ/СТ/ЛТ/ПТ/Арта).
    "vehicleIcon": {
      "visible": true,
      "showSpeaker": false,
      "x": 0,
      "y": -20,
      "alpha": 100,
      "color": null,
      "maxScale": 130,
      "scaleX": 0,
      "scaleY": 16,
      "shadow": {
        "alpha": 100,
        "color": "0x000000",
        "angle": 45,
        "distance": 0,
        "size": 6,
        "strength": 200
      }
    },
    // Индикатор здоровья.
    "healthBar": {
      "visible": true,
      "x": -41,
      "y": -33,
      "alpha": 100,
      "color": null,
      "lcolor": null,
      "width": 80,
      "height": 12,
      "border": {
        "alpha": 30,
        "color": "0x000000",
        "size": 1
      },
      "fill": {
        "alpha": 30
      },
      "damage": {
        "alpha": 80,
        "color": null,
        "fade": 1
      }
    },
    // Floating damage values for ally, player, squadman.
    // Всплывающий урон для союзника, игрока, взводного.
    "damageText": {
      "$ref": { "path":"damageText" }
    },
    "damageTextPlayer": {
      "$ref": { "path":"damageText" }
    },
    "damageTextSquadman": {
      "$ref": { "path":"damageText" }
    },
    // Vehicle contour icon.
    // Контурная иконка танка.
    "contourIcon": {
      "visible": true,
      "x": 6,
      "y": -74,
      "alpha": 100,
      "color": null,
      "amount": 0
    },
    // Player or clan icon.
    "clanIcon": {
      "visible": true,
      "x": -20,
      "y": -71,
      "w": 16,
      "h": 16,
      "alpha": 100
    },
    // Vehicle tier.
    "levelIcon": {
      "visible": true,
      "x": 0,
      "y": -80,
      "alpha": 100
    },
    // Markers "Help!" and "Attack!".
    "actionMarker": {
      "visible": true,
      "x": 0,
      "y": -67,
      "alpha": 100
    },
    // Block of text fields.
    "textFields": [
      // Text field with the name of the player.
      {
        "name": "Tank name",
        "visible": true,
        "x": 0,
        "y": -36,
        "alpha": 100,
        "color": null,
        "font": {
          "name": "$FieldFont",
          "size": 13,
          "align": "center",
          "bold": false,
          "italic": false
        },
        "shadow": {
          "alpha": 100,
          "color": "0x000000",
          "angle": 45,
          "distance": 0,
          "size": 6,
          "strength": 200
        },
        "format": "{{vehicle}}{{turret}}"
      },
      // Text field with the percentage of remaining health.
      // Текстовое поле с процентом оставшегося здоровья.
      {
        "name": "Percent of HP",
        "visible": true,
        "x": 0,
        "y": -20,
        "alpha": 100,
        "color": "0xFCFCFC",
        "font": {
          "name": "$FieldFont",
          "size": 11,
          "align": "center",
          "bold": true,
          "italic": false
        },
        "shadow": {
          "alpha": 100,
          "color": "0x000000",
          "angle": 45,
          "distance": 0,
          "size": 4,
          "strength": 100
        },
        "format": "{{hp-ratio}}%"
      },
      // Text field with wn8 color.
      {
        "name": "Player name",
        "visible": true,
        "x": 0,
        "y": -50,
        "alpha": 100,
        "color": "{{c:wn8}}",
        "font": {
          "name": "$FieldFont",
          "size": 12,
          "align": "center",
          "bold": true,
          "italic": false
        },
        "shadow": {
          "alpha": 100,
          "color": "0x000000",
          "angle": 45,
          "distance": 0,
          "size": 6,
          "strength": 200
        },
        "format": "{{nick}}"
      }
    ]
  }
}